# openpose_ros  
ROS Wrapper for [openpose](https://github.com/CMU-Perceptual-Computing-Lab/openpose)
This package is a fork of [tue-robotics-graveyard/openpose_ros](https://github.com/tue-robotics-graveyard/openpose_ros), and removed some bugs.  

![openpose](openpose.png)


## Supported Platform
NVIDIA GTX 1080Ti  
NVIDIA JETSON TX2  

## Requirements
[openpose](https://github.com/CMU-Perceptual-Computing-Lab/openpose/tree/ca7fd21c0a502b31b64e0fdd7ad6ce3a15fccc73)      
[image_recognition_msgs](https://github.com/tue-robotics/image_recognition/tree/master/image_recognition_msgs) commit c188590ed0af1ff952d4c5be26cfb69bd91fd4ba
    

## Setup  
```bash  
git clone https://github.com/tue-robotics/image_recognition  
git checkout c188590ed0af1ff952d4c5be26cfb69bd91fd4ba  
```

## Usage  
1. Send [image_recognition_msgs/GetPersons](https://github.com/tue-robotics/image_recognition/blob/c188590ed0af1ff952d4c5be26cfb69bd91fd4ba/image_recognition_msgs/srv/GetPersons.srv) services.  
2. Retrieve [image_recognition_msgs/PersonDetection](https://github.com/tue-robotics/image_recognition/blob/c188590ed0af1ff952d4c5be26cfb69bd91fd4ba/image_recognition_msgs/msg/PersonDetection.msg) message. This shows the body part.  

3. The body part element is composed of [image_recognition_msgs/BodyPartDetection](https://github.com/tue-robotics/image_recognition/blob/c188590ed0af1ff952d4c5be26cfb69bd91fd4ba/image_recognition_msgs/msg/BodypartDetection.msg) message. This shows the location of the body part, and the confidence.


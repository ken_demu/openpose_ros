#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Image
from image_recognition_msgs.srv import GetPersons
from image_recognition_msgs.msg import PersonDetection, BodypartDetection


def callPersonService(msg):
    rospy.wait_for_service("detect_poses")
    try:
        detect_poses = rospy.ServiceProxy("detect_poses", GetPersons)
        res = detect_poses(msg)
        print res
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e
        
    
if __name__ == "__main__":
    rospy.init_node("hello")
    rospy.Subscriber("/kodak/image_raw", Image, callPersonService)
    rospy.spin()
